# *Análisis hermenéutico de la fundación de Colima: lugar de encuentro y desencuentro en la historiografía regional*

Aquí se encuentran dos ediciones de la tesis para la 
Licenciatura en Filosofía de la Universidad de Colima:

1. `ucol`. La tesis como fue entregada a la Facultad
de Filosofía de la Universidad de Colima.

2. `co`. La edición posterior realizada por el 
Círculo Ometeotl. El título fue simplificado a
*La fundación de Colima*.

Para la labor de marcado se tomó en cuenta la edición
del Círculo Ometeotl.
